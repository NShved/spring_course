package by.shved.bookstore.repository;

import by.shved.bookstore.dto.Book;

import java.util.List;

public interface IBookRepository {

    List<Book> list();

    Book add(Book book);

    void edit(Book obj);

    void deleteById(long id);

    Book findById(long id);
}
