package by.shved.bookstore.repository.impl;

import by.shved.bookstore.dto.Book;
import by.shved.bookstore.dto.enums.BookGenre;
import by.shved.bookstore.repository.IBookRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepository extends ARepository<Book> implements IBookRepository {

    @PostConstruct
    public void init() {
        Book swbook = new Book();
        swbook.setAuthor("Mathew Stover");
        swbook.setGenre(BookGenre.SCIFI);
        swbook.setTitle("Revenge of the Siths");

        Book dresden = new Book();
        dresden.setTitle("Cold Times");
        dresden.setGenre(BookGenre.FANTASY);
        dresden.setAuthor("Jim Butcher");

        this.add(swbook);
        this.add(dresden);
    }


    @Override
    public List<Book> list() {
        return new ArrayList<>(repoMap.values());
    }

    @Override
    public Book add(Book book) {
        incId();
        book.setId(getId());
        repoMap.put(book.getId(), book);
        return book;
    }

    @Override
    public void edit(Book obj) {
        repoMap.replace(obj.getId(), obj);
    }

    @Override
    public void deleteById(long id) {
        repoMap.remove(id);
    }

    @Override
    public Book findById(long id) {
        return repoMap.get(id);
    }
}
