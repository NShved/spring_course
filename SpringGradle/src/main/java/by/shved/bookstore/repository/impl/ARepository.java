package by.shved.bookstore.repository.impl;

import java.util.HashMap;
import java.util.Map;

public abstract class ARepository<T>{

    private static long id = 0;

    protected Map<Long, T> repoMap = new HashMap<>();

    protected void incId(){
        id++;
    }

    public static long getId() {
        return id;
    }

    public Map<Long, T> getRepoMap() {
        return repoMap;
    }

    public void setRepoMap(Map<Long, T> repoMap) {
        this.repoMap = repoMap;
    }
}
