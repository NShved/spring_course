package by.shved.bookstore.controller;

import by.shved.bookstore.dto.Book;
import by.shved.bookstore.dto.enums.BookGenre;
import by.shved.bookstore.service.IBookService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.Collections;

@Controller
public class BookController {

    private IBookService bookService;

    public BookController(IBookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "books", method = RequestMethod.GET)
    public String showBook(Model model, @RequestParam long bookId){
        model.addAttribute("books", Collections.singletonList(bookService.findById(bookId)));
        return "viewBooks";
    }

    @RequestMapping(value = "/viewBooks", method = RequestMethod.GET)
    public String showBooks(Model model){
        bookService.list().forEach(book -> {System.out.println(book.toString());});
        model.addAttribute("books", bookService.list());
        return "viewBooks";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public String addBook(Model model){
        model.addAttribute("genres", Arrays.asList(BookGenre.values()));
        return "addBook";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(Book book){
        bookService.add(book);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/editBook/{bookId}", method = RequestMethod.GET)
    public ModelAndView editBook(ModelAndView modelAndView, @PathVariable long bookId){
        modelAndView.addObject("command", bookService.findById(bookId));
        modelAndView.setViewName("editBook");
        return modelAndView;
    }

    @RequestMapping(value = "/editBook", method = RequestMethod.POST)
    public String editBook(Book book){
        bookService.edit(book);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/deleteBook/{bookId}", method = RequestMethod.GET)
    public String deleteBookById(@PathVariable long bookId){
        bookService.delete(bookId);
        return "redirect:/viewBooks";
    }
}
