package by.shved.bookstore.dto.enums;

public enum BookGenre {
    DETECTIVE("Detective"),
    HORROR("Horror"),
    SCIFI("SciFi"),
    FANTASY("Fantasy");

    private final String description;

    private BookGenre(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
