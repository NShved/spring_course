package by.shved.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:app.properties")
public class AppProperties {

    @Value("${configuration.debug}")
    private boolean debugMode;

    public boolean isDebugMode() {
        return debugMode;
    }
}
