package by.shved;

import org.springframework.context.ApplicationContext;

public class App {

    private static ApplicationContext context;

    public static void main(String[] args){
        /*
        context = new AnnotationConfigApplicationContext(Config.class);

        BookController bookController = context.getBean("bookController", BookController.class);
        bookController.showBooks();

        bookController.deleteBookById(1);
*/
    }

    private static void checkBeansPresence(String... beans) {
        for (String beanName : beans) {
            System.out.println("Is " + beanName + " in ApplicationContext: " +
                    context.containsBean(beanName));
        }
    }

    private static void allBeans(){
        for (String name: context.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

}
