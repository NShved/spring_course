package by.shved.utils.performance;

import by.shved.config.AppProperties;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
@Aspect
public class PerformanceTracker implements ApplicationContextAware {
    @Autowired
    private AppProperties appProperties;
    private Timer timer;
    private ApplicationContext context;

    public void start(){
        if(appProperties.isDebugMode()){
            timer = context.getBean("timer", Timer.class);
            timer.start();
        }
    }

    public void end(){
        if(appProperties.isDebugMode()){
            timer.end();
        }
    }

    public Duration duration(){
        return timer.performance();
    }

    public Timer getTimer() {
        return timer;
    }
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
