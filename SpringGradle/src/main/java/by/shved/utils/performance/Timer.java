package by.shved.utils.performance;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Timer {

    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    public Duration performance(){
        return Duration.between(endDateTime, startDateTime);
    }

    public void start(){
        startDateTime = LocalDateTime.now();
    }

    public void end(){
        endDateTime = LocalDateTime.now();
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }
}
