package by.shved.utils.performance;

import by.shved.config.AppProperties;
import by.shved.utils.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
@Aspect
public class PerformanceAspect{

    private final Logger logger;
    private final AppProperties appProperties;
    private final Supplier<PerformanceTracker> performanceTrackerSupplier;

    public PerformanceAspect(Logger logger, AppProperties appProperties, Supplier<PerformanceTracker> performanceTrackerSupplier) {
        this.logger = logger;
        this.appProperties = appProperties;
        this.performanceTrackerSupplier = performanceTrackerSupplier;
    }

    @Around("by.shved.utils.aspects.Pointcuts.controllerPointcut()")
    public void applyPerformanceTracker(ProceedingJoinPoint proceedingJoinPoint){

            if(appProperties.isDebugMode()){
                PerformanceTracker performanceTracker = performanceTrackerSupplier.get();
                performanceTracker.start();
                logger.logMessage("Start of: " + proceedingJoinPoint.toShortString() + ", " +  performanceTracker.getTimer().getStartDateTime());

                try {
                    proceedingJoinPoint.proceed();
                } catch (Throwable throwable) {
                    logger.logMessage(throwable.getLocalizedMessage());
                }

                performanceTracker.end();
                logger.logMessage("End of : " + proceedingJoinPoint.toShortString() + ", " + performanceTracker.getTimer().getStartDateTime());
                logger.logMessage("Duration of " + proceedingJoinPoint.toShortString() + ": "  + performanceTracker.duration().toString());
            }

    }
}
