package by.shved.utils.transaction;

import by.shved.utils.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE )
public class TransactTracker {
    private boolean isStarted;
    private Logger logger;

    public TransactTracker(Logger logger) {
        this.logger = logger;
    }

    public void hasStarted(){
        isStarted = true;
        logger.logMessage("Transaction has started");
    }

    public void hasEnded(){
        isStarted = false;
        logger.logMessage("Transaction has ended");
    }
}
