package by.shved.utils.transaction;

import by.shved.config.AppProperties;
import by.shved.utils.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TransactAspect implements ApplicationContextAware {

    private AppProperties appProperties;
    private ApplicationContext context;
    private Logger logger;

    public TransactAspect(AppProperties appProperties, Logger logger) {
        this.appProperties = appProperties;
        this.logger = logger;
    }

    @Around("by.shved.utils.aspects.Pointcuts.repositoryPointcut()")
    public void applyTransactTracker(ProceedingJoinPoint proceedingJoinPoint){
        if(appProperties.isDebugMode()){
            TransactTracker tracker = context.getBean("transactTracker", TransactTracker.class);
            tracker.hasStarted();

            try {
                proceedingJoinPoint.proceed();
            } catch (Throwable throwable) {
                logger.logMessage("Transaction has failed");
            }

            tracker.hasEnded();
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
