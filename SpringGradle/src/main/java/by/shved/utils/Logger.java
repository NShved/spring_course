package by.shved.utils;

import by.shved.config.AppProperties;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Logger {
    @Autowired
    private AppProperties appProperties;

    public void logMessage(String msg){
        if (appProperties.isDebugMode()){
            System.out.println(msg);
        }
    }
}
