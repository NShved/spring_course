package by.shved.utils.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Pointcuts {
    @Pointcut("execution(* by.shved.bookstore.controller.*.*(..))")
    public static void controllerPointcut(){}

    @Pointcut("within(by.shved.bookstore.repository.impl..*)")
    public void repositoryPointcut(){}

}
