<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Edit Book</h1>
<form:form method="POST" action="/SpringGradle/editBook">
     <table >
         <tr>
            <td></td>
            <td><form:hidden  path="id" /></td>
         </tr>
         <tr>
            <td>Title : </td>
            <td><form:input path="title"  /></td>
          </tr>
          <tr>
            <td>Author :</td>
            <td><form:input path="author" /></td>
          </tr>

          <tr>
            <td> </td>
            <td><input type="submit" value="Save" /></td>
          </tr>
     </table>
</form:form>