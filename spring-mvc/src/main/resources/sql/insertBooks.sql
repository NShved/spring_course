INSERT INTO Book(id, title, author, desc)
VALUES(1, 'Turncoat', 'Jim Butcher', 'Warden Morgan came to Dresden for help');
INSERT INTO Book(id, title, author, desc)
VALUES(2, 'Wordbearers', 'Some author of W40K', 'Space marine-priests come for Heresy');
INSERT INTO Book(id, title, author, desc)
VALUES(3, 'Iron hands', 'Another W40K author', 'Another space marine legion fights Heresy');