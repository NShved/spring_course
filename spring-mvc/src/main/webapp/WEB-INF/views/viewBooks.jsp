<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Book List</h1>
<table border="2" width="70%" cellpadding="2">
    <tr><th>Id</th><th>Name</th><th>Description</th>

        <security:authorize access="hasRole('ROLE_ADMIN')">
            <th>Edit</th><th>Delete</th>
        </security:authorize>

        <th>View book</th></tr>

    <c:forEach var="book" items="${books}">
        <tr>
            <td>${book.id}</td>
            <td>${book.title}</td>
            <td>${book.author}</td>
            <td>${book.desc}</td>
            <security:authorize access="hasRole('ROLE_ADMIN')">
                <td><a href="editBook/${book.id}">Edit</a></td>
                <td><a href="deleteBook/${book.id}">Delete</a></td>
            </security:authorize>
            <td><a href="books?bookId=${book.id}">View book</a></td>
        </tr>
    </c:forEach>
</table>
<br/>
        <security:authorize access="hasRole('ROLE_ADMIN')">
        <a href="addBook">Add Book</a>
        </security:authorize>
        <a href="viewBooks">Back</a>