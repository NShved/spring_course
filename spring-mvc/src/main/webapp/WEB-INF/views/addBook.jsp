<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Add New Book</h1>
<form method="POST">
     <table >
          <tr>
               <td>Title : </td>
               <td><input type="text" name="title"  /></td>
          </tr>
          <tr>
               <td>Author :</td>
               <td><input type="text" name="author"/></td>
          </tr>
          <tr>
               <td>Description :</td>
               <td><input type="text" name="desc"/></td>
          </tr>
          <tr>
               <td></td>
               <td><input type="submit" value="addBook"/></td>
          </tr>
          <tr>
               <td></td>
               <td><input type="hidden"
                          name="${_csrf.parameterName}"
                          value="${_csrf.token}" /></td>
          </tr>
     </table>
</form>