<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book shop</title>
</head>


<body>
<h1><p>Hello there!</p></h1>

<security:authorize access="hasRole('ROLE_ADMIN')">
    <a href="addBook">Add Book</a>
</security:authorize>
<a href="viewBooks">View Books</a>

</body>