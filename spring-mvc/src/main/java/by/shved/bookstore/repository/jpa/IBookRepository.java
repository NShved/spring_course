package by.shved.bookstore.repository.jpa;

import by.shved.bookstore.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IBookRepository extends JpaRepository<Book, Long> {
}
