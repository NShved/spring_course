package by.shved.bookstore.repository;

import by.shved.security.users.User;

public interface IUserRepositry {

    User addUser(User user);

    User getUserByLogin(String login);
}
