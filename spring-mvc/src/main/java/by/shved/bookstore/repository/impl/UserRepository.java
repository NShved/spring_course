package by.shved.bookstore.repository.impl;

import by.shved.bookstore.repository.IUserRepositry;
import by.shved.security.users.Roles;
import by.shved.security.users.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Repository
public class UserRepository implements IUserRepositry {

    private Map<String, User> repoMap = new HashMap<>();

    @PostConstruct
    public void init(){
        List<GrantedAuthority> adminRoles = Arrays.asList(
                new SimpleGrantedAuthority(Roles.ADMIN),
                new SimpleGrantedAuthority(Roles.USER)
        );
        User admin = new User(
                "admin",
                "admin",
                adminRoles
        );

        List<GrantedAuthority> userRoles = Collections.singletonList(
                new SimpleGrantedAuthority(Roles.USER)
        );
        User user = new User(
                "user",
                "user",
                userRoles
        );

        addUser(admin);
        addUser(user);
    }

    @Override
    public User addUser(User user) {
        repoMap.put(user.getUsername(), user);
        return user;
    }

    @Override
    public User getUserByLogin(String login) {
        return repoMap.get(login);
    }
}
