package by.shved.bookstore.controller;

import by.shved.bookstore.model.Book;
import by.shved.bookstore.service.IBookService;
import by.shved.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;

@Controller
public class BookController {

    private IBookService bookService;

    @Autowired
    public BookController(IBookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String showBook(Model model, @RequestParam long bookId){
        model.addAttribute("books", Collections.singletonList(bookService.findById(bookId)));
        return "viewBooks";
    }

    @RequestMapping(value = "/viewBooks", method = RequestMethod.GET)
    public String showBooks(Model model){
        model.addAttribute("books", bookService.list());
        return "viewBooks";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public ModelAndView addBook(ModelAndView modelAndView){
        modelAndView.setViewName("addBook");
        return modelAndView;
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(@Validated Book book, Errors errors){
        if(errors.hasErrors()){
            throw new ValidationException(errors.toString());
        }
        bookService.add(book);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/editBook/{bookId}", method = RequestMethod.GET)
    public ModelAndView editBook(ModelAndView modelAndView, @PathVariable long bookId){
        modelAndView.addObject("command", bookService.findById(bookId));
        modelAndView.setViewName("editBook");
        return modelAndView;
    }

    @RequestMapping(value = "/editBook", method = RequestMethod.POST)
    public String editBook(@Validated Book book, Errors errors){
        if(errors.hasErrors()){
            throw new ValidationException(errors.toString());
        }
        bookService.edit(book);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/deleteBook/{bookId}", method = RequestMethod.GET)
    public String deleteBookById(@PathVariable long bookId){
        bookService.delete(bookId);
        return "redirect:/viewBooks";
    }
}
