package by.shved.bookstore.service.impl;

import by.shved.bookstore.repository.IUserRepositry;
import by.shved.bookstore.service.IUserService;
import by.shved.security.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    private IUserRepositry userRepositry;

    @Override
    public User getUserByLogin(String login) {
        return userRepositry.getUserByLogin(login);
    }
}
