package by.shved.bookstore.service;

import by.shved.bookstore.model.Book;

import java.util.List;

public interface IBookService {

    List<Book> list();

    Book add(Book book);

    void delete(long id);

    void edit(Book book);

    Book findById(long id);
}
