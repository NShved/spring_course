package by.shved.bookstore.service;

import by.shved.security.users.User;

public interface IUserService {
    User getUserByLogin(String login);
}
