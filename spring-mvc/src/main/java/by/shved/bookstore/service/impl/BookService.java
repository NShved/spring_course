package by.shved.bookstore.service.impl;

import by.shved.bookstore.model.Book;
import by.shved.bookstore.repository.jpa.IBookRepository;
import by.shved.bookstore.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService implements IBookService {
    private IBookRepository bookRepository;

    @Autowired
    public BookService(IBookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> list() {
        return bookRepository.findAll();
    }

    @Override
    public Book add(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public void delete(long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void edit(Book book) {
        if(bookRepository.findById(book.getId()).isPresent()){
            bookRepository.save(book);
        } else {
            System.out.println("There is no book with such ID " + book.getId());
        }
    }

    @Override
    public Book findById(long id) {
        return bookRepository.findById(id).get();
    }
}
