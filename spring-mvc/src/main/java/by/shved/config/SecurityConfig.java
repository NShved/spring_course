package by.shved.config;

import by.shved.security.UserServiceDetailsImpl;
import by.shved.security.users.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public UserDetailsService userDetailsService(){
        return new UserServiceDetailsImpl();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("user").password("user").roles("USER")
                .and().withUser("admin").password("admin").roles("ADMIN");
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/addBook").hasAuthority(Roles.ADMIN)
                .antMatchers("/editBook/**").hasAuthority(Roles.ADMIN)
                .antMatchers("/deleteBook/**").hasAuthority(Roles.ADMIN)
                .antMatchers("/home").permitAll()
                .antMatchers("/error").permitAll()
                .antMatchers("/viewBooks").hasAuthority(Roles.USER)
                .antMatchers("/viewBooks").hasAuthority(Roles.ADMIN)
//                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .httpBasic()
                .realmName("bookShop")
                .and()
                .rememberMe()
                .tokenValiditySeconds(2419200)
                .key("bookShopKey")
                .and()
                .logout()
                .logoutSuccessUrl("/");
    }
}
