package by.shved.security;

import by.shved.bookstore.service.IUserService;
import by.shved.security.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class UserServiceDetailsImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userService.getUserByLogin(login);
        UserDetails userDetails;

        if(user != null){
            userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getAuthorities());
        } else {
            userDetails = new org.springframework.security.core.userdetails.User("", "", Collections.EMPTY_SET);
        }

        return userDetails;
    }
}
