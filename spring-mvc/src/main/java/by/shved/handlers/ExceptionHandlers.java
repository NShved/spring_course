package by.shved.handlers;

import by.shved.bookstore.controller.BookController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(assignableTypes = {BookController.class})
public class ExceptionHandlers {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleCannotDeleteException(Exception exp){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("errorMessage", exp.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }


}
